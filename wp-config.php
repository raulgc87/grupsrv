<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'grupservator' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '.D7`D90;nHRgxQ&JT6KOYc:L.e11(_c&:0~JOMt@Tc](@Z4??=G3[Tl>a%7(!wQJ' );
define( 'SECURE_AUTH_KEY',  '$m?F3[u)FwOLh|9zD?| }1/>ze)w`loJn5lZhzI;]i)K[uu:)TTh<rOR8B7{k[xt' );
define( 'LOGGED_IN_KEY',    'KPMQVQ9IJ%-fY)78yjo3&@&cx,1k@:N$8G 6nU0GF:F{WE:_V7j[P>1?/evjJ#JS' );
define( 'NONCE_KEY',        '.~*#F4L0M6TH(Dank5Sp|gII.{394=b>`BtoD/N=ec;oBO{[yb3().qs,v&JpLet' );
define( 'AUTH_SALT',        'w>wfW,N]_#!6V{V?B@.E?--jvZ|SLf<l9$a `ggVr bsNV2jT+8/8.Fkk0Lh7_46' );
define( 'SECURE_AUTH_SALT', 'Mpy6N+^2G+L,jxc=K=y|(p,q=8y;>qx&dqXa9&kr`A`{K#BmB@MQco?h>sn1h-D[' );
define( 'LOGGED_IN_SALT',   ';PEo yOG3GKhB14+(ewA5ds/EPT+B#u|9df[A5+.Q2A/YEVbgmy.-H#sF6!#,3+P' );
define( 'NONCE_SALT',       '!{z|$j#negs]/hA-v&4od|p@ MX;/BucnsqyCzc!T}@i%.%[N*dTu98c^WsYI[1j' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
