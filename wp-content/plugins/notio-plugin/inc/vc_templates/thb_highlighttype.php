<?php function thb_highlighttype( $atts, $content = null ) {
	$atts = vc_map_get_attributes( 'thb_highlighttype', $atts );
	extract( $atts );

	$out             = '';
	$text            = '';
	$element_id      = uniqid( 'thb-highlighttype-' );
	$slide_text_safe = vc_value_from_safe( $slide_text );

	$slide_text_safe = thb_remove_vc_added_p( $slide_text_safe );

	$class[] = 'thb-highlighttype';
	$class[] = 'true' === $thb_highlight_animation ? 'thb-highlight-hover' : false;
	$class[] = $extra_class;
	$class[] = $animation;
	ob_start();
	?>
	<div id="<?php echo esc_attr( $element_id ); ?>" class="<?php echo esc_attr( implode( ' ', $class ) ); ?>">
		<?php
		if ( preg_match_all( '/(\*.*?\*)/is', $slide_text_safe, $entries ) ) {
			foreach ( $entries[0] as $entry ) {
				$highlighttype    = substr( $entry, 1, -1 );
				$slide_text_safe  = str_replace( $entry, '<placeholder>', $slide_text_safe );
				$slide_text_toadd = '<span class="thb-highlight">' . $highlighttype . '</span>';
			}
		}
			echo str_replace( '<placeholder>', $slide_text_toadd, $slide_text_safe );
		?>

		<style>
			<?php if ( $thb_text_color ) { ?>
				#<?php echo esc_attr( $element_id ); ?>.thb-highlighttype {
					color: <?php echo esc_attr( $thb_text_color ); ?>;
				}
			<?php } ?>
			<?php if ( $thb_highlight_color ) { ?>
				#<?php echo esc_attr( $element_id ); ?>.thb-highlighttype .thb-highlight {
					background-image: linear-gradient( <?php echo esc_attr( $thb_highlight_color ); ?>,  <?php echo esc_attr( $thb_highlight_color ); ?>);
				}
			<?php } ?>
			<?php if ( $thb_highlight_height ) { ?>
				#<?php echo esc_attr( $element_id ); ?>.thb-highlighttype .thb-highlight {
					background-size: 0% <?php echo esc_attr( $thb_highlight_height ); ?>%;
				}
			<?php } ?>
		</style>

	</div>

	<?php
	$out = ob_get_clean();

	return $out;
}
thb_add_short( 'thb_highlighttype', 'thb_highlighttype' );
