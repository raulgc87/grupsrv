<?php function thb_testimonial_parent( $atts, $content = null ) {
	$atts = vc_map_get_attributes( 'thb_testimonial_parent', $atts );
	extract( $atts );

	$element_id = 'thb-testimonials-' . wp_rand( 10, 99 );
	$el_class[] = 'thb-testimonials';
	$el_class[] = 'slick';
	$el_class[] = $thb_style;

	$columns = 'testimonial-style3' === $thb_style ? $columns : '1';
	$fade    = 'testimonial-style3' === $thb_style ? false : 'true';
	$out     = '';
	ob_start();
	?>
	<div id="<?php echo esc_attr( $element_id ); ?>" class="<?php echo esc_attr( implode( ' ', $el_class ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" data-pagination="true" data-fade="<?php echo esc_attr( $fade ); ?>">
		<?php echo wpb_js_remove_wpautop( $content, false ); ?>
	</div>
	<?php
	$out = ob_get_clean();
	return $out;
}
thb_add_short( 'thb_testimonial_parent', 'thb_testimonial_parent' );
