<?php
function thb_instagram( $atts, $content = null ) {
	esc_html_e( 'Instagram Functionalities have been removed. Please use a 3rd party Instagram Plugin.', 'notio' );
}
thb_add_short( 'thb_instagram', 'thb_instagram' );
