<?php function thb_post( $atts, $content = null ) {
	$style = '';
	$atts  = vc_map_get_attributes( 'thb_post', $atts );
	extract( $atts );

	if ( $offset ) {
		$source .= '|offset:' . $offset; }
	$source_data   = VcLoopSettings::parseData( $source );
	$query_builder = new ThbLoopQueryBuilder( $source_data );
	$thb_posts     = $query_builder->build();
	$thb_posts     = $thb_posts[1];
	$style         = $style === '' ? 'style6-alt' : $style;

	$classes[] = 'posts-shortcode align-stretch';
	$classes[] = 'blog-listing-' . $style;
	$classes[] = $style !== 'style6' ? 'row' : '';
	$classes[] = $style === 'style2' ? 'masonry' : '';
	ob_start();
	?>
	<div class="<?php echo esc_attr( implode( ' ', $classes ) ); ?>">
		<?php
		$i = 0;
		if ( $thb_posts->have_posts() ) :
			while ( $thb_posts->have_posts() ) :
				$thb_posts->the_post();
				set_query_var( 'thb_i', $i );
				set_query_var( 'thb_columns', $columns );
				get_template_part( 'inc/templates/blogbit/' . $style );
				$i++;
			endwhile;
		endif;
		?>
	</div>
	<?php
	$out = ob_get_clean();

	wp_reset_postdata();

	return $out;
}
thb_add_short( 'thb_post', 'thb_post' );
