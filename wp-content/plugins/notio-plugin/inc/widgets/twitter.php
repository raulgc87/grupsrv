<?php
// thb latest Posts w/ Images
class widget_thb_twitterwidget extends WP_Widget {
	function __construct() {
		$widget_ops = array(
			'classname'   => 'widget_thb_twitterwidget',
			'description' => __( 'Display your Tweets', 'notio' ),
		);

		parent::__construct(
			'thb_thb_twitterwidget_widget',
			esc_html__( 'Fuel Themes - Twitter', 'notio' ),
			$widget_ops
		);

		$this->defaults = array(
			'title'    => '',
			'username' => 'fuel_themes',
			'show'     => 5,
		);
	}

	function widget( $args, $instance ) {
		$params = array_merge( $this->defaults, $instance );

		// Before Widget.
		echo $args['before_widget'];

		// Title.
		if ( $params['title'] ) {
			echo wp_kses_post( $args['before_title'] . apply_filters( 'widget_title', $params['title'], $instance, $this->id_base ) . $args['after_title'] );
		}
		$tweets = thb_gettweets( $params['show'], $params['username'] );
		if ( array_key_exists( 'errors', $tweets ) ) {
			echo esc_html( $tweets['errors'][0]['message'] );
			return;
		}
		$twitter_data = thb_twitter_data( $tweets );

		foreach ( $twitter_data['tweets'] as $tweet ) {
			?>
				<div class="thb_tweet">
					<p><?php echo wp_kses_post( $tweet['text'] ); ?></p><a href="https://twitter.com/<?php echo esc_html( $twitter_data['screen_name'] ); ?>/status/<?php echo esc_attr( $tweet['id'] ); ?>" class="thb_tweet_time" target="_blank"><?php echo esc_html( $tweet['time'] ); ?></a>
				</div>
			<?php
		}
		echo $args['after_widget'];
	}
	function update( $new_instance, $old_instance ) {
		$instance = $new_instance;

		return $instance;
	}
	function form( $instance ) {
		$params = array_merge( $this->defaults, $instance );
		?>
			<!-- Title -->
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_html_e( 'Title', 'notio' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $params['title'] ); ?>" /></p>

			<!-- Username -->
			<p><label for="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>"><?php esc_html_e( 'Username:', 'notio' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'username' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'username' ) ); ?>" type="text" value="<?php echo esc_attr( $params['username'] ); ?>" /></p>

		<p>
				<label for="<?php echo esc_attr( $this->get_field_id( 'show' ) ); ?>"><?php esc_html_e( 'Number of Tweets:', 'notio' ); ?></label>
				<input id="<?php echo esc_attr( $this->get_field_id( 'show' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'show' ) ); ?>" value="<?php echo esc_attr( $params['show'] ); ?>" class="widefat" />
			</p>
		<?php
	}
}
