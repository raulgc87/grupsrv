<?php

function thb_register_widgets() {
	$thb_widgets = array(
		'twitter' => 'widget_thb_twitterwidget',
	);
	foreach ( $thb_widgets as $key => $value ) {
		require_once notio_plugin()->get_plugin_path() . 'inc/widgets/' . sanitize_key( $key ) . '.php';
		register_widget( $value );
	}
}

add_action( 'widgets_init', 'thb_register_widgets' );
